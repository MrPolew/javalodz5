package domowe1;

import java.util.Scanner;

public class If1 {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        System.out.print("Proszę podaj liczbę. Sprawdzimy czy jest parzysta: ");
        int check = input.nextInt();

        if(check % 2 == 0)
            System.out.println("Twoja liczba jest parzysta!");
        else
            System.out.println("Twoja liczba jest nieparzysta");
    }
}
