package domowe1;

import java.util.Scanner;

public class If2 {
    public static void main(String[] args) {
        int check;

        Scanner scanner = new Scanner(System.in);

        System.out.print("Wprowadz liczbę dla której wypiszemy wartość bezwzględną: ");
        check = scanner.nextInt();

        if(check < 0){
            System.out.println(Math.abs(check));
        } else {
            System.out.println(check);
        }


    }
}
