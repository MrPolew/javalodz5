package domowe1;

import java.util.Scanner;

public class If3 {
    public static void main(String[] args) {
        int x1, x2, x3, temp;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj 1 liczbe:");
        x1 = scanner.nextInt();
        System.out.println("Podaj 2 liczbe:");
        x2 = scanner.nextInt();
        System.out.println("Podaj 3 liczbe:");
        x3 = scanner.nextInt();

        if (x1 > x2 && x1 > x3) {
            System.out.println("Największa liczba to x1 : " + x1);
        } else if (x2 > x1 && x2 > x3) {
            System.out.println("Największa liczba to x2 : " + x2);
        } else {
            System.out.println("Największa liczba to x3 : " + x3);
        }
    }
}
