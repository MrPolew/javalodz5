package domowe1;

import java.util.Scanner;

public class If4 {

    public static void main(String[] args) {

        int punkty;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Wprowadz ilość punktów uzyskaną z testu: ");
        punkty = scanner.nextInt();

        if (punkty >= 50){
            if(punkty >= 50 && punkty < 60){
                System.out.println("Zaliczyłeś na 2");
            }
            else if (punkty >= 60 && punkty < 70) {
                System.out.println("Zaliczyłeś na 3");
            }
            else if (punkty >= 70 && punkty < 80) {
                System.out.println("Zaliczyłeś na 4");
            }
            else if (punkty >= 80 && punkty < 90) {
                System.out.println("Zaliczyłeś na 5");
            }
            else if (punkty >= 90 && punkty <= 100) {
                System.out.println("Zaliczyłeś na 6");
            }
            else {
                System.out.println("Wyszedles poza skale !!");
            }

        } else {
            System.out.println("Nie zaliczyłeś testu");
        }
    }
}
