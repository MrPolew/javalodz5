package domowe1;

public class Loops1 {
    public static void main(String[] args) {

        System.out.println("Ten program wypisuje liczby od 1 do 100 za pomocą trzech róznych pętli");

        System.out.println("Pętla for");

        for (int i = 1; i <= 100 ; i++) {
            System.out.print(i + " ");
        }

        System.out.println();

        System.out.println("Pętla while");
        int licznik = 1;

        while (licznik <= 100) {
            System.out.print(licznik + " ");
            licznik++;
        }
        System.out.println();

        System.out.println("Pętla do while");
        int i = 1;

        do {
            System.out.print(i + " ");
            i++;
        } while (i <= 100 );
    }
}
