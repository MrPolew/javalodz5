package domowe1;

public class Loops2 {
    public static void main(String[] args) {
        System.out.println("Program wypisujący liczby miedzy 15 a 100 podzielna przez 17");

        for (int i = 15; i < 100; i++) {
            if(i % 17 == 0)
                System.out.println(i);
        }
    }
}
