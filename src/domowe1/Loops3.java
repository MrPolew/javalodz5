package domowe1;

import java.util.Scanner;

public class Loops3 {
    public static void main(String[] args) {
        System.out.println("Program zlicza wprowadzone liczby calkowite dodatnie.");
        System.out.println("Aby zakonczyc program wpisz 0");

        Scanner scanner = new Scanner(System.in);
        int liczba, licznik = 0;
        while((liczba = scanner.nextInt()) != 0) {
            if(liczba > 0)
                licznik++;
        }
        System.out.println("Wprowadziles tyle liczb dodatnich: " + licznik);
    }
}
