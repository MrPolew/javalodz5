package domowe1;

import java.util.Scanner;

public class Loops4 {
    public static void main(String[] args) {
        System.out.println("Program pobiera 10 liczb calkowitych i zlicza ile z nich jest dodatnich i parzystych");

        Scanner scanner = new Scanner(System.in);
        int licznik = 0;

        for (int i = 0; i < 10; i++) {
            System.out.print("Podaj liczbe: ");
            int liczba = scanner.nextInt();
            if(liczba > 0 && liczba % 2 == 0)
                licznik++;
        }

        System.out.println("Tyle jest liczb parzystych i wiekszych od zera: " + licznik);
    }
}
