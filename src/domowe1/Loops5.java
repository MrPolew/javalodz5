package domowe1;

import java.util.Scanner;

public class Loops5 {
    public static void main(String[] args) {
        System.out.println("Program zlicza sume liczb z przedzialu a i b podanych przez uzytkownika");

        int x, y, suma = 0;

        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj dolna granicę: ");
        x = scanner.nextInt();
        System.out.print("Podaj górną granicę: ");
        y = scanner.nextInt();

        for (; x <= y; x++) {
            suma += x;
        }

        System.out.println("Suma wynosi : " + suma);
    }
}
