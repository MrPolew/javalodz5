package domowe1;

import java.util.Scanner;

public class Loops6 {
    public static void main(String[] args) {
        System.out.println("Program sprawdza czy znasz wynik mnozenia");

        int x, y, wynik, zgaduj;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj liczbę x : ");
        x = scanner.nextInt();
        System.out.println("Podaj liczbę y : ");
        y = scanner.nextInt();

        wynik = x * y;

        System.out.println("Teraz zgaduj wynik :)");

        while((zgaduj = scanner.nextInt()) != wynik) {
            System.out.println("Nie zgadłeś!!. Próbuj dalej");
        }

        System.out.println("Zgadłeś!! Wynik mnożenia x: " + x + " i y: " + y + " wynosi : " + wynik);
    }
}
