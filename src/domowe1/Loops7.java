package domowe1;

import java.util.Scanner;

public class Loops7 {

    public static void main(String[] args) {
        System.out.println("Podany program sprawdza czy podana liczba jest liczbą pierwszą");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Wprowadź liczbę do sprawdzenia: ");
        int liczba = scanner.nextInt();


        if (liczba < 2) {
            System.out.println("Nie jest pierwsza");
        }
            else{
                for (int i = 2; i <= Math.sqrt(liczba); i++) {
                    if(liczba % i == 0) {
                        System.out.println("Nie jest pierwsza");
                        break;
                } else {
                        System.out.println("Jest pierwsza");
                        break;
                    }
            }
        }
    }
}
