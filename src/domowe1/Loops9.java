package domowe1;

import java.util.Scanner;

public class Loops9 {

    public static void main(String[] args) {
        System.out.println("Program wypisuje n poteg naturalnych liczby 2");

        System.out.println("Ile potęg obliczyc dla 2 : ");
        Scanner scanner = new Scanner(System.in);
        int potega = scanner.nextInt();
        int dwojka = 2;

        for (int i = 1; i <= potega; i++) {


            System.out.println("Dwojka do potegi " + i + " wynosi: " + dwojka);
            dwojka *= 2;
        }
    }
}
