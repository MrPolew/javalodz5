package domowe1;

import java.util.Scanner;

/**
 * Created by Polew on 14.10.2017.
 */
public class Silnia {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Dla jakiej liczby chcesz wyznaczyc silnie : ");
        int liczba = input.nextInt();
        int licznik = 1, wynik = 1;

        while(licznik <= liczba) {
            wynik *= licznik;
            licznik++;
        }

        System.out.println("Silnia dla " + liczba + " wynosi: " + wynik);
    }
}
