package domowe1;

import java.util.Random;
import java.util.Scanner;

public class Tablice1 {

    public static void main(String[] args) {
        int count = 0;

        System.out.println("Program wczytuje liczbę T i liczy wystapienia jej w tablicy o okreslonym" +
                " przez Cb rozmiarze");

        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        System.out.println("Podaj rozmiar tablicy tab: ");
        int size = scanner.nextInt();
        int[] tablica = new int[size];

        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = random.nextInt(100);
        }

        System.out.println("Podaj liczbę która chcesz wyszukać: ");
        int liczbaSzukana = scanner.nextInt();

        for (int i = 0; i < tablica.length; i++) {
            if(tablica[i] == liczbaSzukana){
                count++;
            }
        }

        System.out.println("Liczba " + liczbaSzukana + " występuje " + count + " razy");
    }
}
