package domowe1;

import java.util.Random;

public class Tablice3 {

    public static void main(String[] args) {

        int[] tablica = new int[10];
        Random random = new Random();

        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = random.nextInt(10) + 1;
        }

        for (int i : tablica) {
            System.out.println(i);
        }

        for (int i = 1; i <= tablica.length; i++) {
            int counter = 0;
            for (int j = 0; j < 10; j++) {
                if (tablica[j] == i) {
                    counter++;
                }
            }
            System.out.println("Element " + i + " wystąpił " + counter + " razy");
        }
    }
}
