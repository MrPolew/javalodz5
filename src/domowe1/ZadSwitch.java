package domowe1;

import java.util.Scanner;

public class ZadSwitch {
    public static void main(String[] args) {

        String kod;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj kod języka: ");
        kod = scanner.nextLine();

        switch (kod.toUpperCase()) {
            case "PL":
                System.out.println("Dzień dobry. Witam serdeccznie");
                break;
            case "EN":
                System.out.println("Good morning. Welcome!");
                break;
            case "DE":
                System.out.println("Guten Morgen. Herzlich Wilkommen!");
                break;
            case "FR":
                System.out.println("Bonne matin. Bienvenue!");
                break;
            default:
                System.out.println("Nie wprowadziłeś poprawnego kodu...");
                System.out.println("Good morning. Welcome!");
        }
    }
}
