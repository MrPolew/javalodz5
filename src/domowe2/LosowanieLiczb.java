package domowe2;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

public class LosowanieLiczb {

    public static int losojCalkowita(int a, int b) {
        Random rand = new Random();
        return rand.nextInt(b - a + 1) + a;
    }

    public static double losojRzeczywista(double a, double b) {
        Random rand = new Random();
        return rand.nextDouble() * (b - a) + a;
    }

    public static double losojRzeczywista(double a, double b, int ile) {
        Random rand = new Random();
        double d = rand.nextDouble() * (b - a) + a;
        BigDecimal bd = new BigDecimal(d).setScale(ile, RoundingMode.CEILING);
        d = bd.doubleValue();
        return d;
    }

    public static void losojTabliceCalkowita(int a, int b) {
        int[] tab = new int[b - a];
        Random rand = new Random();
        for (int i = 0; i < tab.length; i++) {
            tab[i] = losojCalkowita(a, b);
        }
        System.out.println(tab[rand.nextInt(tab.length)]);
    }

    public static void losojTabliceRzeczywista(double a, double b) {
        double[] tab = new double[(int)(b - a)];
        Random rand = new Random();
        for (int i = 0; i < tab.length; i++) {
            tab[i] = losojRzeczywista(a, b);
        }
        System.out.println(tab[rand.nextInt(tab.length)]);
    }

    public static boolean porownanieTablicRozmiar(int[] tab1, int[] tab2) {
        if(tab1.length != tab2.length) return false;
        return true;
    }

    public static boolean porownanieTablic(int[] tab1, int[] tab2) {
        if(porownanieTablicRozmiar(tab1, tab2)){
            for (int i = 0; i < tab1.length; i++) {
                if(tab1[i] != tab2[i]){
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public static boolean porownanie2D(int[][] tab1, int[][] tab2) {
        for (int i = 0; i < tab1.length; i++) {
            //porownanieTablic();
            for (int j = 0; j < tab1[i].length ; j++) {
                porownanieTablic(tab1[j], tab2[j]);
            }
        }

        return true;
    }

    public static boolean porownanieTablicRozmiar(int[] tab1, double[] tab2) {
        if(tab1.length != tab2.length) return false;
        return true;
    }

    public static void main(String[] args) {
        System.out.println(losojCalkowita(19, 29));
        System.out.println(losojRzeczywista(0.0, 5.0));
        System.out.println(losojRzeczywista(0.0, 5.0, 5));
        losojTabliceCalkowita(10, 20);
        losojTabliceRzeczywista(10, 20);

        int[] tab1 = new int[9];
        int[] tab2 = new int[9];

        int[] tab3 = {1, 2, 3, 4, 5};
        int[] tab4 = {1, 2, 3, 4, 5};

        System.out.println(porownanieTablicRozmiar(tab1, tab2));
        System.out.println(porownanieTablic(tab3, tab4));
    }
}
