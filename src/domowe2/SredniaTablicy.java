package domowe2;

import java.util.Random;
import java.util.Scanner;

public class SredniaTablicy {

    public static void main(String[] args) {

        int suma = 0, mniejsze = 0, wieksze = 0;


        System.out.println("Program liczy srednia liczb z tablicy.");

        System.out.println("Podaj wielkość tablicy: ");
        Scanner scanner = new Scanner(System.in);
        int wielkosc = scanner.nextInt();
        int[] table = new int[wielkosc];

        Random random = new Random();
        for (int i = 0; i < table.length; i++) {
            table[i] = random.nextInt(20) + 1;
        }

        for (int i = 0; i < table.length; i++) {
            suma += table[i];
        }

        for (int i : table) {
            System.out.print(i + ", ");
        }

        double srednia = suma / table.length;
        System.out.println("Srednia wynosi: " + srednia);

        for (int i = 0; i < table.length; i++) {
            if(table[i] < srednia) {
                mniejsze++;
            } else if(table[i] > srednia) {
                wieksze++;
            }
        }

        System.out.println("W tablicy jest tyle elementów mniejszych od średniej: " + mniejsze);
        System.out.println("W tablicy jest tyle elementów większych od średniej: " + wieksze);
    }
}
