package domowe2;

import java.util.Arrays;
import java.util.Random;

public class Statystyka {

    public static void main(String[] args) {

        System.out.println("Program wypisuje statystyke występowania liczb w losowej" +
                " 20 elementowej tablicy.");

        int[] table = new int[20];
        Random random = new Random();

        for (int i = 0; i < table.length; i++) {
            table[i] = random.nextInt(10) + 1;
        }

        Arrays.sort(table);
        System.out.println(Arrays.toString(table));

        for (int i = 1; i <= 10; i++) {
            int licznik = 0;
            for (int j = 0; j < table.length; j++) {
                if(i == table[j]) licznik++;
            }
            System.out.println("Liczba " + i + " występuje " + licznik + " razy.");
        }
    }
}
