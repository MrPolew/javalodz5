package domowe2;

import java.util.Random;
import java.util.Scanner;

public class Wyszukiwarka {

    public static void main(String[] args) {
        System.out.println("Program wyszukuje ilosc wystąpień liczby x w tablicy");

        System.out.println("Podaj wielkość tablicy: ");
        Scanner scanner = new Scanner(System.in);
        int wielkosc = scanner.nextInt();
        int[] table = new int[wielkosc];

        Random random = new Random();
        for (int i = 0; i < table.length; i++) {
            table[i] = random.nextInt(20) + 1;
        }

        System.out.println("Podaj jaka liczbę chcesz wyszukac w tablicy: ");
        int szukanaLiczba = scanner.nextInt();
        int count = 0;

        for (int i : table) {
            if (i == szukanaLiczba) count++;
            System.out.print(i + ", ");
        }

        System.out.println("Podana liczba występuje " + count + " razy");
    }
}
