package domowe3;

public class Complex {
    private int real;
    private int image;

    public Complex(int real, int image) {
        this.real = real;
        this.image = image;
    }

    public int getReal() {
        return real;
    }

    public int getImage() {
        return image;
    }

    public void setReal(int real) {
        this.real = real;
    }

    public void setImage(int image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return ("" + real + " + " +  image + "i" );
    }

    public static Complex dodawanie(Complex z1, Complex z2) {
        Complex w = new Complex(z1.real + z2.real, z1.image + z2.image);
        return w;
    }

    public static Complex odejmowanie(Complex z1, Complex z2) {
        Complex w = new Complex(z1.real - z2.real, z1.image - z2.image);
        return w;
    }

    public static Complex mnozenie(Complex z1, Complex z2) {
        Complex w = new Complex(z1.real * z2.real - z1.image * z2.image, z2.real * z1.image + z1.real * z2.image);
        return w;
    }

    public static void main(String[] args) {
        Complex z1 = new Complex(4,5);
        Complex z2 = new Complex(6,8);
        System.out.println("Dodawanie:");
        Complex z3 = dodawanie(z1, z2);
        System.out.println(z3);
        System.out.println("Odejmowanie:");
        z3 = odejmowanie(z1, z2);
        System.out.println(z3);
        System.out.println("Mnozenie:");
        z3 = mnozenie(z1, z2);
        System.out.println(z3);

    }

}

