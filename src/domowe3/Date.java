package domowe3;

public class Date {
    int day, month, year;

    public Date() {
        day = 1;
        month = 1;
        year = 2001;
    }

    public Date(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    @Override
    public String toString() {
        String day = Integer.toString(this.day),
                month = Integer.toString(this.month),
                year = Integer.toString(this.year);
        if (this.day < 10) {
            day = "0" + this.day;
        }
        if (this.month < 10) {
            month = "0" + this.month;
        }
        if (this.year < 1000) {
            year = "0" + this.year;
        }

        return day + "/" + month + "/" + year;
    }

    public static void main(String[] args) {
        Date date1 = new Date();
        Date date2 = new Date(20, 10, 1990);

        System.out.println(date1);
        System.out.println(date2);
    }
}
