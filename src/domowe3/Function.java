package domowe3;

public class Function {
    private int a, b, c, x;

    public Function() {
        a = 0;
        b = 0;
        c = 0;
        x = 0;
    }

    public Function(int a, int b, int c, int x) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.x = x;
    }

    public static double getResult(Function f) {
        return f.a * f.x * f.x + f.b * f.x + f.c;
    }

    public static void getZeroPlaces(Function f) {
        double pdelta, x0, x1, x2;
        int delta = (f.b * f.b - 4 * f.a * f.c);
        pdelta = Math.sqrt(delta);
        if (f.a == 0) {
            System.out.println("Funkcja nie jest kwadratowa");
        } else if (delta > 0) {
            x1 = (-f.b - pdelta) / (2 * f.a);
            x2 = (-f.b + pdelta) / (2 * f.a);
            System.out.println("Delta wynosi: " + delta + "\nX1 wynosi: " + x1 + "\nX2 wynosi: " + x2);
        } else if (delta == 0) {
            x0 = -f.b / (2 * f.a);
            System.out.println("Delta wynosi:" + delta + "\nX0 wynosi: " + x0);
        } else {
            System.out.println("Delta jest ujemna i wynosi: " + delta + ". Funkcja nie posiada miejsc zerowych");
        }
    }

    public static void main(String[] args) {
        Function f1 = new Function(2, 4, 2, 10);
        Function f2 = new Function();
        System.out.println(getResult(f1));
        getZeroPlaces(f1);
        System.out.println();
        System.out.println(getResult(f2));
        getZeroPlaces(f2);
    }
}
