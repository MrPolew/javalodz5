package podstawy1;

import java.util.Locale;
import java.util.Scanner;

/**
 * Created by Polew on 14.10.2017.
 */
public class Calculator1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.US);
        double x, y;

        System.out.println("To jest kalkulator podaj, teraz 2 liczby!");
        System.out.println("Podaj liczbę x : ");
        x = scanner.nextDouble();
        System.out.println("Podaj liczbę y : ");
        y = scanner.nextDouble();

        System.out.println("x = " + x + " y = " + y);
        System.out.println("x + y = " + (x + y));
        System.out.println("x - y = " + (x - y));
        System.out.println("x * y = " + (x * y));
        System.out.println("x / y = " + (x / y));

    }
}
