package podstawy1;

import java.util.*;

/**
 * Created by Polew on 14.10.2017.
 */
public class CalculatorIf {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.US);
        double x, y;

        System.out.println("To jest kalkulator podaj, teraz 2 liczby!");
        System.out.println("Podaj liczbę x : ");
        x = scanner.nextDouble();
        scanner.nextLine();
        System.out.println("Podaj liczbę y : ");
        y = scanner.nextDouble();
        scanner.nextLine();

        System.out.println("Co chcesz zrobić? (= - * /)");
        String dzialanie = scanner.next();
        scanner.close();
        System.out.println("x = " + x + " y = " + y);

        if (dzialanie.equals("+")) {
            System.out.println("x + y = " + (x + y));
        } else if (dzialanie.equals("-")) {
            System.out.println("x - y = " + (x - y));
        } else if (dzialanie.equals("*")) {
            System.out.println("x * y = " + (x * y));
        } else if (dzialanie.equals("/")) {
            System.out.println("x / y = " + (x / y));
        } else {
            System.out.println("Podałeś nieprawidlowy symbol!!");
        }

    }
}
