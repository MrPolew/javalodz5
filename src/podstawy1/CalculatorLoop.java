package podstawy1;

import java.util.Locale;
import java.util.Scanner;

/**
 * Created by Polew on 14.10.2017.
 */
public class CalculatorLoop {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.US);
        double x, y;
        String warunek = "";


        do {
            System.out.println("To jest kalkulator podaj, teraz 2 liczby!");
            System.out.println("Podaj liczbę x : ");
            x = scanner.nextDouble();
            scanner.nextLine();
            System.out.println("Podaj liczbę y : ");
            y = scanner.nextDouble();
            scanner.nextLine();
            System.out.println("x = " + x + " y = " + y);
            System.out.println("Co chcesz zrobić? (= - * /) lub N - wyjscie z pętli");
            String dzialanie = scanner.next();
            switch (dzialanie) {
                case "+":
                    System.out.println("x + y = " + (x + y));
                    break;
                case "-":
                    System.out.println("x - y = " + (x - y));
                    break;
                case "*":
                    System.out.println("x * y = " + (x * y));
                    break;
                case "/":
                    System.out.println("x / y = " + (x / y));
                    break;
                case "N":
                    warunek = "N";
                    break;
                default:
                    System.out.println("Podałeś nieprawidlowy symbol!!");
                    break;
            }
        } while(!warunek.equals("N"));
        scanner.close();
    }
}
