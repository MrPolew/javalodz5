package podstawy1;

/**
 * Created by Polew on 14.10.2017.
 */
public class Odlicz1 {

    public static void main(String[] args) throws InterruptedException {
        odlicz1();
        odlicz2();
    }

    public static void odlicz1() throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            System.out.println("Instrukcja nr: " + i);
            Thread.sleep(1000);
        }
    }

    public static void odlicz2() throws InterruptedException {
        for (int i = 10; i >= 0; i--) {
            System.out.println("Instrukcja nr: " + i);
            Thread.sleep(1000);
        }
    }
}




