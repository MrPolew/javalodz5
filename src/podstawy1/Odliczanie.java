package podstawy1;

import java.util.Scanner;

/**
 * Created by Polew on 14.10.2017.
 */
public class Odliczanie {

    public static void main(String[] args) throws InterruptedException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Program wyswietla liczby od podanej przez Cb do zera\nPodaj liczbę od której ma odliczać: ");


        for (int granica = scanner.nextInt(); granica >= 0; granica--) {
            System.out.println("i = " + granica);
            Thread.sleep(1000);
        }

    }
}
