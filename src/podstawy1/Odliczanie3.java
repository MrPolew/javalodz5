package podstawy1;

/**
 * Created by Polew on 14.10.2017.
 */

public class Odliczanie3 {

    public static void main(String[] args) throws InterruptedException {
        odlicz1(10);
        odlicz2(10);
    }

    public static void odlicz1(int ile) throws InterruptedException {
        for (int i = 0; i <= ile; i++) {
            System.out.println("Instrukcja nr: " + i);
            Thread.sleep(1000);
        }
    }

    public static void odlicz2(int ile) throws InterruptedException {
        for (; ile >= 0; ile--) {
            System.out.println("Instrukcja nr: " + ile);
            Thread.sleep(1000);
        }
    }
}
