package podstawy1;

/**
 * Created by Polew on 14.10.2017.
 */
public class Typy {
    public static void main(String[] args) {
        System.out.println('a' + 'A');
        System.out.println("a" + "A");
        System.out.println(1+2);
        System.out.println(1.0+2.0);
        System.out.println("cudzyslow \"");
        System.out.println(true);
        System.out.println("abc" + 't');
        System.out.println("abc" + 1234);
        System.out.println(true + "abc");
    }
}
