package podstawy1;

import java.util.Scanner;

/**
 * Created by Polew on 14.10.2017.
 */
public class Zad8 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int suma = 0, liczba;
        int max = 0, min = 0;
        System.out.println("Podaj 10 liczb: ");
        for(int i = 0; i < 10; i++) {
            System.out.println("Wprowadz " + (i+1) + "-ta liczbę: ");
            liczba = input.nextInt();

            if (i == 0) {
                min = liczba;
                max = liczba;
            } else {
                if (liczba < min) {
                    min = liczba;
                }
                if (liczba > max) {
                    max = liczba;
                }
            }
        }
        System.out.println("Suma wynosi: " + suma);
        System.out.println("Srednia wynosi: " +(double)(suma/10));
        System.out.println("Max wynosi: " + max);
        System.out.println("Min wynosi: " + min);
    }
}
