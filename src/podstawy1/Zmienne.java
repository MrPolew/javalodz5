package podstawy1;

/**
 * Created by Polew on 14.10.2017.
 */
public class Zmienne {

    public static void main(String[] args) {
        int a = 80;
        int b = 20;

        System.out.println("Operator porównania dla a = " + a + " b = " + b + " a == b = " + (a==b));
        System.out.println("Operator dodawania dla a = " + a + " b = " + b + " a + b = " + (a + b));
        System.out.println("Operator odejmowania dla a = " + a + " b = " + b + " a - b = " + (a - b));
        System.out.println("Operator mnozenia dla a = " + a + " b = " + b + " a * b = " + (a * b));
        System.out.println("Operator dzielenia dla a = " + a + " b = " + b + " a / b = " + (a / b));
        System.out.println("Operator modulo dla a = " + a + " b = " + b + " a % b = " + (a % b));
    }
}
