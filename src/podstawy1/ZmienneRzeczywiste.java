package podstawy1;

/**
 * Created by Polew on 14.10.2017.
 */
public class ZmienneRzeczywiste {

    public static void main(String[] args) {
        double a = 80.0;
        double b = 20.0;

        System.out.println("Operator porównania dla a = " + a + " b = " + b + " a == b = " + (a==b));
        System.out.println("Operator dodawania dla a = " + a + " b = " + b + " a + b = " + (a + b));
        System.out.println("Operator odejmowania dla a = " + a + " b = " + b + " a - b = " + (a - b));
        System.out.println("Operator mnozenia dla a = " + a + " b = " + b + " a * b = " + (a * b));
        System.out.println("Operator dzielenia dla a = " + a + " b = " + b + " a / b = " + (a / b));
        System.out.println("Operator modulo dla a = " + a + " b = " + b + " a % b = " + (a % b));
        System.out.println("Kilka operatorów dla a = " + a + " b = " + b + " a + b * a / a + b % a = " + (a + b * a / a + b % a));
        System.out.println(a/0);
    }
}
