package podstawy2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Tablice tablice = new Tablice();
        double[] tab = new double[10];
        tablice.losuj(tab);
        tablice.wypisz(tab);
        System.out.printf("Suma: %.2f", tablice.suma(tab));
        System.out.println();
        System.out.printf("Srednia: %.2f", tablice.srednia(tab));
        System.out.println();
        System.out.printf("Maks: %.2f", tablice.maksimum(tab));
        System.out.println();
        System.out.printf("Min: %.2f", tablice.minimum(tab));

        System.out.println();
        System.out.println();
        System.out.println("Klasa obliczanie:");
        Obliczenia obliczenia = new Obliczenia();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj pierwszą liczbę: ");
        double x = scanner.nextDouble();
        System.out.println("Podaj drugą liczbę: ");
        double y = scanner.nextDouble();
        System.out.println("Suma: " + obliczenia.suma(x, y));
        System.out.println("Srednia: " + obliczenia.srednia(x, y));
        System.out.println("Maksimum: " + obliczenia.maksimum(x, y));
        System.out.println("Minimum: " + obliczenia.minimum(x, y));
    }
}
