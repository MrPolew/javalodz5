package podstawy2;

public class Obliczenia {

    public double suma(double x, double y) {
        return x + y;
    }

    public double srednia(double x, double y) {
        return (x + y) / 2;
    }

    public double maksimum(double x, double y) {
        if(x > y) return x;
        return y;
    }

    public double minimum(double x, double y) {
        if(x < y) return x;
        return y;
    }

}
