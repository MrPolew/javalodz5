package podstawy2;

import java.util.Random;

public class PasswordGenerator {
    private int[] password;
    private char[] charTab;
    Random random = new Random();

    public PasswordGenerator() {
        password = new int[8];
        password[0] = random.nextInt(90 - 65) + 65;
        for (int i = 1; i < password.length - 2; i++) {
            password[i] = random.nextInt(122 - 97) + 97;
        }
        password[6] = random.nextInt(47 - 33) + 33;
        password[7] = random.nextInt(57 - 48) + 48;
    }

    public PasswordGenerator(int howManyChars) {
        password = new int[howManyChars];
        password[0] = random.nextInt(90 - 65) + 65;
        for (int i = 1; i < password.length - 2; i++) {
            password[i] = random.nextInt(122 - 97) + 97;
        }
        password[password.length - 2] = random.nextInt(47 - 33) + 33;
        password[password.length-1] = random.nextInt(57 - 48) + 48;
    }

    public char[] createCharArray(int index) {
        charTab = new char[index];
        for (int i = 0; i < password.length; i++) {
            charTab[i] = (char) password[i];
        }
        return charTab;
    }

    public static void showPassword(char[] tab){
        for (char ch: tab){
            System.out.print(ch);
        }
    }

    public int[] getPassword() {
        return password;
    }

    public static void main(String[] args) {

        PasswordGenerator pass1 = new PasswordGenerator();
        showPassword(pass1.createCharArray(pass1.getPassword().length));
        System.out.println();
        PasswordGenerator pass2 = new PasswordGenerator(20);
        showPassword(pass2.createCharArray(pass2.getPassword().length));
    }
}
