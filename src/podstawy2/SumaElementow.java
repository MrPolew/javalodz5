package podstawy2;

import java.util.Random;

public class SumaElementow {
    // Program tworzy tablice ktorej kazdy element jest suma poprzednich elementow.
    // 2 pierwsze elementy sa losowe.

    public static void main(String[] args) {
        int[] tab = new int[20];
        Random random = new Random();
        tab[0] = random.nextInt(10);
        tab[1] = random.nextInt(10);
        for (int i = 2; i < tab.length; i++) {
            tab[i] = tab[i-1] + tab[i-2];
        }

        for (int i: tab) {
            System.out.print("[" + i + "]");
        }
    }
}
