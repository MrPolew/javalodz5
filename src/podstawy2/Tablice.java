package podstawy2;

import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

public class Tablice {

    /*
    •Utwórz klasę Tablice,
    •Utwórz w niej następujące metody, wykonujące operacje na całej tablicy liczb rzeczywistych (przekazanej jako parametr) i zwracające pojedynczą liczbę rzeczywistą: suma, srednia, minimum, maksimum.
    •Utwórz w niej następujące metody odpowiadające za inicjowanie tablicy: pobierz, losujoraz metodę wypisz, odpowiedzialną za eleganckie wydrukowanie zawartości tablicy.
    */

    public double[] pobierz(double[] tab) {
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.US);

        for (int i = 0; i < tab.length; i++) {
            System.out.println("Wpisz " + (i + 1) + " liczbę:");
            tab[i] = scanner.nextDouble();
        }
        return tab;
    }

    public double[] losuj(double[] tab) {
        Random random = new Random();
        for (int i = 0; i < tab.length; i++) {
            tab[i] = random.nextDouble() * 100;
        }
        return tab;
    }

    public void wypisz(double[] tab) {
        for (double d : tab) {
            System.out.printf("[%.2f]", d);
        }
        System.out.println();
    }

    public double suma(double[] tab) {
        double suma = 0;
        for (int i = 0; i < tab.length; i++) {
            suma += tab[i];
        }
        return suma;
    }

    public double srednia(double[] tab) {
        return suma(tab) / tab.length;
    }

    public double maksimum(double[] tab) {
        double max = 0, temp;
        for (int i = 0; i < tab.length; i++) {
            if (i == 0) {
                max = tab[i];
            }
            temp = tab[i];
            if (temp > max) max = temp;
        }
        return max;
    }

    public double minimum(double[] tab) {
        double min = 0, temp;
        for (int i = 0; i < tab.length; i++) {
            if (i == 0) {
                min = tab[i];
            }
            temp = tab[i];
            if (temp < min) min = temp;
        }
        return min;
    }

}
