package podstawy2;

import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

public class TabliceDwuwymiarowe {

    public double[][] pobierz(double[][] tab) {
        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.US);

        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[i].length; j++) {
                System.out.println("Wpisz [" + (i + 1) + "][" + (j + 1) + "] liczbę:");
                tab[i][j] = scanner.nextDouble();
            }
        }
        return tab;
    }

    public double[][] losuj(double[][] tab) {
        Random random = new Random();
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[i].length; j++) {
                tab[i][j] = random.nextDouble() * 10;
            }
        }
        return tab;
    }

    public void wypisz(double[][] tab) {
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[i].length; j++) {
                System.out.printf("[%.2f]", tab[i][j]);
            }
            System.out.println();
        }
        System.out.println();
    }

    public double suma(double[][] tab) {
        double suma = 0;
        for (int i = 0; i < tab.length; i++) {
            for (int j = 0; j < tab[i].length; j++) {
                suma += tab[i][j];
            }
        }
        return suma;
    }

    public double sumaPrzekatnej(double[][] tab) {
        double suma = 0;
        for (int i = 0; i < tab.length; i++) {
            for (int j = i; j == i; j++) {
                if (tab.length == tab[i].length) {
                    suma += tab[i][j];
                }
            }
        }
        return suma;
    }

    public void wypiszDodatnie(double[][] tab) {
        for (int i = 0; i < tab.length; i++) {
            boolean isPositive = true;
            for (int j = 0; j < tab[i].length; j++) {
                if (tab[i][j] < 0) {
                    isPositive = false;
                }
            }
            if (isPositive) {
                for (int j = 0; j < tab[i].length; j++) {
                    System.out.printf("[%.2f]", tab[i][j]);
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    public void dodawanieMacierzy(double[][] tab1, double[][] tab2, double[][] tabWynik) {
        if (tab1.length != tab2.length) {
            System.out.println("Nie można dodać. Liczba kolumn się nie zgadza");
            return;
        } else {
            for (int i = 0; i < tab1.length; i++) {
                for (int j = 0; j < tab2.length; j++) {
                    if (tab1[i].length != tab2[i].length) {
                        System.out.println("Nie można dodać. Liczba wierszy się nie zgadza");
                        return;
                    } else {
                        tabWynik[i][j] = tab1[i][j] + tab2[i][j];
                    }
                }
            }
        }
        System.out.println("Suma macierzy:");
        wypisz(tabWynik);
    }

    public void mnozenieMacierzy(double[][] tab1, double[][] tab2, double[][] tabWynik) {
        System.out.println("Podane macierze muszą mieć:\n" +
                "*liczba wierszy tab1 = liczbie kolumn tab2 i na odwrot\n" +
                "*macierz wynikowa ma liczbe wierszy tab1 i liczbe kolumn tab2");
        for (int i = 0; i < tabWynik.length; i++) {
            for (int j = 0; j < tabWynik[i].length; j++) {
                if (tabWynik.length != tab1.length && tabWynik[i].length != tab2[i].length) {
                    System.out.println("Macierz wynikowa ma zły rozmiar");
                    return;
                } else {
                    double suma = 0;
                    for (int k = 0; k < tab1[i].length; k++) {
                        suma += tab1[i][k] * tab2[k][j];
                    }
                    tabWynik[i][j] = suma;
                }
            }
        }
        System.out.println("Mnożenie macierzy macierzy:");
        wypisz(tabWynik);
    }


    public static void main(String[] args) {

        /*double[][] tab1 = new double[3][3];
        double[][] tab2 = new double[4][3];
        double[][] tabWynik = new double[3][3];
        TabliceDwuwymiarowe tabliceDwuwymiarowe = new TabliceDwuwymiarowe();
        tabliceDwuwymiarowe.losuj(tab1);
        tabliceDwuwymiarowe.losuj(tab2);
        tabliceDwuwymiarowe.wypisz(tab1);
        tabliceDwuwymiarowe.wypisz(tab2);
        tabliceDwuwymiarowe.dodawanieMacierzy(tab1, tab2, tabWynik);*/

        double[][] tab1 = new double[4][2];
        double[][] tab2 = new double[2][4];
        double[][] tabWynik = new double[4][4];
        TabliceDwuwymiarowe tabliceDwuwymiarowe = new TabliceDwuwymiarowe();
        tabliceDwuwymiarowe.losuj(tab1);
        tabliceDwuwymiarowe.losuj(tab2);
        tabliceDwuwymiarowe.wypisz(tab1);
        tabliceDwuwymiarowe.wypisz(tab2);
        tabliceDwuwymiarowe.mnozenieMacierzy(tab1, tab2, tabWynik);

        /*tabliceDwuwymiarowe.pobierz(tab);
        tabliceDwuwymiarowe.wypisz(tab);
        System.out.println("Suma : " + tabliceDwuwymiarowe.suma(tab));
        System.out.println("Suma przekątnej: " + tabliceDwuwymiarowe.sumaPrzekatnej(tab));*//*
        tabliceDwuwymiarowe.wypiszDodatnie(tab);*/

    }
}
