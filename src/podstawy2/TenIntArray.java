package podstawy2;

import java.util.Random;

public class TenIntArray {
    // Program tworzy 10 elementowa tablice intow po czym zamienia je w niej miejscami

    public static void main(String[] args) {
        int[] tab = new int[9];
//        int[] temp = new int[9];
        Random random = new Random();

        for (int i = 0; i < tab.length; i++) {
            tab[i] = random.nextInt(10);
        }

        System.out.println("Tablica przed odwróceniem:");
        for (int i: tab) {
            System.out.print("["+i+"}");
        }
        System.out.println();

        for (int i = 0; i < tab.length / 2; i++) {
            int temp;
            int skad = i;
            int dokad = tab.length - i - 1;

            temp = tab[skad];
            tab[skad] = tab[dokad];
            tab[dokad] = temp;
        }

        /*
        Moja interpretacja
        for (int i = 0, j = tab.length - 1; i < tab.length; i++, j--) {
            temp[i] = tab[j];
        }

        tab = temp;*/

        System.out.println("Tablica po odwróceniu");
        for (int i: tab) {
            System.out.print("["+i+"}");
        }
    }
}
