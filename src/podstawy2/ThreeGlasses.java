package podstawy2;

import java.util.Scanner;

public class ThreeGlasses {

    public static void GlassChange(int x, int y) {
        int temp = x;
        x = y;
        y = temp;
        System.out.println("Zmienna x po zmianie: " + x);
        System.out.println("Zmienna y po zmianie: " + y);
    }

    public static void main(String[] args) {
        // Program wczytuje dwie liczby od uzytkownika i zamienia ich miejsce w zmiennych

        Scanner scanner = new Scanner(System.in);
        int x, y;

        System.out.println("Wprowadz liczbe x:");
        x = scanner.nextInt();
        System.out.println("Wprowadz liczbe y:");
        y = scanner.nextInt();
        System.out.println("Zmienna x:" + x);
        System.out.println("Zmienna y:" + y);

        System.out.println("Zamieniamy miejsce");
        GlassChange(x, y);
    }
}
