package podstawy2;

import java.util.Scanner;

public class Zad1 {

    public static void main(String[] args) {

        String[] imiona = new String[5];

        System.out.println("Program pobiera 5 imion do tablicy i je wyswietla.");
        Scanner input = new Scanner(System.in);

        for (int i = 0; i < imiona.length; i++) {
            System.out.println("Podaj imie nr " + (i + 1) + ": ");
            imiona[i] = input.nextLine();
        }

        System.out.println();

        for(String imie: imiona) {
            System.out.println("Witaj " + imie);
        }
    }
}
