package podstawy2;

import java.util.Arrays;
import java.util.Scanner;

public class Zad2 {

    public static void main(String[] args) {

        System.out.println("Podaj ilosc elementow ktore ma przechowywac tablica typu int: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] tab = new int[n];

        for (int i = 0; i < tab.length; i++) {
            System.out.print("Podaj liczbe nr " + (i + 1) + ": ");
            int liczba = scanner.nextInt();
            tab[i] = liczba;
        }

        System.out.println(Arrays.toString(tab));
    }
}
