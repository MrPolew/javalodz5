package podstawy2;

import java.util.Random;
import java.util.Scanner;

public class Zad3 {
    // Program tworzy tablice typu int i wypelnia ja losowymi liczbami.

    public static void main(String[] args) {

        System.out.println("Podaj ilosc elementow ktore ma przechowywac tablica typu int: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] tab = new int[n];
        Random random = new Random();

        for (int i = 0; i < tab.length; i++) {
            tab[i] = random.nextInt(100);
        }

        for (int i: tab) {
            System.out.print(i + ", ");
        }
    }
}
