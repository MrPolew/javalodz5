package podstawy2;

import java.util.Random;
import java.util.Scanner;

public class Zad4 {
    /// Program wyszukuje min, max i sume z losowej tablicy int.
    public static void main(String[] args) {

        System.out.println("Podaj ilosc elementow ktore ma przechowywac tablica typu int: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] tab = new int[n];
        Random random = new Random();
        int sum = 0, min = 0, max = 0, countN = 0, countP = 0;

        for (int i = 0; i < tab.length; i++) {
            tab[i] = random.nextInt(100);
            sum += tab[i];

            if(i == 0) min = tab[0];
            if(max < tab[i]) max = tab[i];

            if(i == 0) max = tab[0];
            if(min > tab[i]) min = tab[i];

            if(tab[i] % 2 == 0){
                countP++;
            } else {
                countN++;
            }
        }

        for (int i : tab) {
            System.out.print(i + ", ");
        }
        System.out.println();
        System.out.println("Suma liczb w tablicy wynosi: " + sum);
        System.out.println("Max : " + max);
        System.out.println("Min : " + min);
        System.out.println("Nieparzyste: " + countN);
        System.out.println("Parzyste: " + countP);
    }
}
