package podstawy2;

import java.util.Random;

public class Zad5 {
    // Program wylosowuje 2 tablice i sumuje je w 3

    public static void main(String[] args) {
        int[] tab1 = new int[10];
        int[] tab2 = new int[10];
        int[] tab3 = new int[10];
        Random random = new Random();

        for (int i = 0; i < tab1.length; i++) {
            tab1[i] = random.nextInt(20);
        }

        for (int i = 0; i < tab1.length; i++) {
            tab2[i] = random.nextInt(20);
        }

        for (int i = 0; i < tab3.length; i++) {
            tab3[i] = tab1[i] + tab2[i];
        }

        for (int i : tab3) {
            System.out.print("[" + i + "]");
        }
    }
}
