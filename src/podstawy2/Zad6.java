package podstawy2;

import java.util.Random;
import java.util.Scanner;

public class Zad6 {
    // Losuje tablice typu double z przedzialu a nastepnie ja wyswietla

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        double[] tab = new double[10];
        System.out.println("Podaj przedział");
        System.out.print("Poczatek przedzialu : ");
        int p = scanner.nextInt();
        System.out.print("Koniec przedzialu : ");
        int k = scanner.nextInt();

        if(p == 0){
            p++;
            k++;
        }

        for (int i = 0; i < tab.length; i++) {
            tab[i] = random.nextDouble() * (k - p) + p;
        }

        for (double d : tab){
            System.out.printf("[%.2f]", d);
        }
    }
}
