package podstawy3;

public class Obliczenia {

    public static final int MIN = 0;
    public static final int MAX = 20;

    private static double ogranicz(double a){
        while (a > MAX) {
            a = a - (MAX - MIN);
        }
        while (a < MIN) {
            a = a + (MAX - MIN);
        }
        return a;
    }

    public static double suma(double a, double b) {
        return ogranicz(a + b);
    }

    public static double srednia(double a, double b) {
        return ogranicz(suma(a, b) / 2);
    }

    public static double miniumum(double a, double b) {
        if (a <= b) {
            return ogranicz(a);
        } else {
            return ogranicz(b);
        }
    }

    public static double maksimum(double a, double b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }

    public static void main(String[] args) {
        System.out.println(suma(12, 30));
        System.out.println(suma(-8, -5));
        System.out.println(suma(8, 0));
        System.out.println(suma(-7, 27));
    }

}
