package podstawy3;

public class Obliczenia3 {

    private  int min;
    private  int max;

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    private  double ogranicz(double a){
        while (a > max) {
            a = a - (max - min + 1);
        }
        while (a < min) {
            a = a + (max - min + 1);
        }
        return a;
    }

    public double suma(double a, double b) {
        return ogranicz(a + b);
    }

    public double srednia(double a, double b) {
        return ogranicz(suma(a, b) / 2);
    }

    public double miniumum(double a, double b) {
        if (a <= b) {
            return ogranicz(a);
        } else {
            return ogranicz(b);
        }
    }

    public double maksimum(double a, double b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
    }

    public static void main(String[] args) {

        Obliczenia3 o1, o2;
        o1 = new Obliczenia3();

        o1.setMin(-15);
        o1.setMax(15);

        System.out.println(o1.suma(12, 30));
        System.out.println(o1.suma(-8, -5));
        System.out.println(o1.suma(8, 0));
        System.out.println(o1.suma(-7, 27));
    }
}
