package podstawy3;

public class Rectangle {

    private Point p1, p2, p3, p4;

    public Rectangle() {
        p1 = new Point(0, 0);
        p2 = new Point(4, 0);
        p3 = new Point(4, 4);
        p4 = new Point(0, 4);
    }

    public Rectangle(int x1, int y1, int x2, int y2) {
        p1 = new Point(x1, y1);
        p2 = new Point(x2, y1);
        p3 = new Point(x2, y2);
        p4 = new Point(x1, y2);
    }

    public Point getP1() {
        return p1;
    }

    public void setP1(Point p1) {
        this.p1 = p1;
    }

    public Point getP2() {
        return p2;
    }

    public void setP2(Point p2) {
        this.p2 = p2;
    }

    public Point getP3() {
        return p3;
    }

    public void setP3(Point p3) {
        this.p3 = p3;
    }

    public Point getP4() {
        return p4;
    }

    public void setP4(Point p4) {
        this.p4 = p4;
    }


    public double getWidth() {
        return p1.distance(p2);
    }

    public double getHeight() {
        return p2.distance(p3);
    }

    public String toString() {
        return "{" + p1 + ", " + p2 + "} -> {" + p3 + ", " + p4 + "}";
    }

    public boolean equals(Rectangle r) {
        return (getWidth() == r.getWidth() && getHeight() == r.getHeight());
    }

    public double area() {
        return getWidth() * getHeight();
    }

    public double circuit() {
        return 2 * (getWidth() + getHeight());
    }

    public boolean isSquare() {
        return (getWidth() == getHeight());
    }
}
