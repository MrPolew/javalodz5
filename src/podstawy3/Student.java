package podstawy3;

public class Student {

    private final static int ZAKRES_MAX = 10;

    private String firstName;
    private String lastName;
    private int age;
    private int[] oceny = new int[ZAKRES_MAX];

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int[] getOceny() {
        return oceny;
    }

    public void setOceny(int[] oceny) {
        this.oceny = oceny;
    }

    public Student() {
        firstName = "";
        lastName = "";
        age = 00;
    }

    public Student(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public Student(Student s) {
        this.firstName = s.firstName;
        this.lastName = s.firstName;
        this.age = s.age;
    }

    @Override
    public String toString() {
        if (lastName.equals("") || firstName.equals("") || age == 00) {
            return "Dane studenta są niekompletne";
        } else {
            return "Student{" +
                    "firstName='" + firstName + '\'' +
                    ", lastName='" + lastName + '\'' +
                    ", age='" + age + '\'' +
                    '}';
        }
    }

    public double srednia(int[] oceny){
        double suma = 0;
        for (int i = 0; i < oceny.length; i++) {
            suma += oceny[i];
        }
        return suma / oceny.length;
    }

    
}
