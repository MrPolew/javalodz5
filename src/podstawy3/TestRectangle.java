package podstawy3;

import java.util.Scanner;

public class TestRectangle {

    private static String menu = "Cześć. Co chcesz zrobić?\n" +
            "1. Wprowadź dane prostokąta 1\n" +
            "2. Zmień lewy-górny punkt\n" +
            "3. Zmień prawy-górny punkt\n" +
            "4. Zmień lewy-dolny punkt\n" +
            "5. Zmień prawy-dolny punkt\n" +
            "6. Pole prostokąta\n" +
            "7. Obwód prostokąta\n" +
            "0. Zakończ\n";

    public static void main(String[] args) {

        Scanner cin = new Scanner(System.in);
        Rectangle r = new Rectangle();
        int choice;

        do {
            System.out.print(menu);
            choice = cin.nextInt();

            switch (choice) {
                case 1:
                    System.out.println("prawy-dolny p1(x , y) = ");
                    r.setP1(new Point(cin.nextInt(), cin.nextInt()));
                    System.out.println("lewy-dolny p2(x , y) = ");
                    r.setP2(new Point(cin.nextInt(), cin.nextInt()));
                    System.out.println("lewy-gorny p3(x , y) = ");
                    r.setP3(new Point(cin.nextInt(), cin.nextInt()));
                    System.out.println("prawy-gorny p4(x , y) = ");
                    r.setP4(new Point(cin.nextInt(), cin.nextInt()));
                    System.out.println(r);
                    break;
                case 2:
                    System.out.println("lewy-górny punkt = ");
                    r.setP1(new Point(cin.nextInt(), cin.nextInt()));
//                    System.out.println("lewy-górny Y = ");
//                    r.setP2(new Point(cin.nextInt(), cin.nextInt()));
                    System.out.println(r);
                    break;
                case 3:
                    System.out.println("prawy-górny punkt = ");
                    r.setP2(new Point(cin.nextInt(), cin.nextInt()));
//                    System.out.println("prawy-górny Y = ");
//                    r.setY3(cin.nextInt());
                    System.out.println(r);
                    break;
                case 4:
                    System.out.println("lewy-dolny punkt = ");
                    r.setP3(new Point(cin.nextInt(), cin.nextInt()));
//                    System.out.println("lewy-dolny Y = ");
//                    r.setY4(cin.nextInt());
                    System.out.println(r);
                    break;
                case 5:
                    System.out.println("prawy-dolny punkt = ");
                    r.setP3(new Point(cin.nextInt(), cin.nextInt()));
//                    System.out.println("prawy-dolny Y = ");
//                    r.setY2(cin.nextInt());
                    System.out.println(r);
                    break;
                case 6:
                    System.out.println("Pole prostokąta: " + r.area());
                    break;
                case 7:
                    System.out.println("Obwód prostokąta: " + r.circuit());
                    break;
                case 0:
                    System.out.println("KONIEC !!!");
                    break;
            }

        } while (choice > 0);
    }
}
