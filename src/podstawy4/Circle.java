package podstawy4;

import java.util.Random;

public class Circle {

    // POLA

    private Point p1;
    private double r;

    // KONSTRUKTORY

    public Circle() {
        p1 = new Point();
        r = 1;
    }

    public Circle(int x, int y, int r) {
        p1 = new Point(x, y);
        this.r = r;
    }

    public Circle (Point p, int r) {
        this.p1 = new Point(p);
        this.r = r;
    }

    public Circle (Circle c) {
        p1 = new Point(c.p1);
        r = c.r;
    }

    // GETTERRY


    public Point getP1() {
        return p1;
    }

    public double getR() {
        return r;
    }

    // SETTERY


    public void setP1(Point p1) {
        this.p1 = p1;
    }

    public void setR(double r) {
        this.r = r;
    }

    // METODY

    public double diameter() {
        return 2 * r;
    }

    public double area() {
        return (Math.PI * r * r);
    }

    public double circuit() {
        return (2 * Math.PI * r);
    }

    public boolean isPointInCircle(Point p) {
        if((p.getX() >= (p1.getX() - r) && p.getX() <= (p1.getX() + r))
                && ((p.getY() >= p1.getY() - r) && (p.getY() <= p1.getY() + r)))
            return true;
        return false;
    }

    public boolean arePointsInCircle(Point[] pTab){
        for (int i = 0; i < pTab.length; i++) {
            isPointInCircle(pTab[i]);
        }
        return false;
    }

    public boolean isInside(Point p) {
        return p1.distance(p) <= r;
    }

    public boolean isInside(Point[] points) {
        for(Point a : points) {
            if (! isInside(a)) return false;
        }
        return true;
    }

    public boolean isOverlapped(Circle c) {
        return p1.distance(c.p1) <= r + c.r;
    }

    // TESTER

    public static void main(String[] args) {
        Circle c1 = new Circle();
        Circle c2 = new Circle(2, 2, 1);
        Random random = new Random();

        System.out.println("Srednica kola nr 1: " + c1.diameter());
        System.out.println("Srednica kola nr 2: " + c2.diameter());
        System.out.println("Pole powierzchni koła nr1: " + c1.area());
        System.out.println("Pole powierzchni koła nr2: "+ c2.area());
        System.out.println("Obwód koła nr1: " + c1.circuit());
        System.out.println("Obwód koła nr2: " + c2.circuit());

        Point p1 = new Point(2,1);
        Point[] tab = {new Point(10, 0),
        new Point(2,2),
        new Point(3,3)};

        System.out.println("Czy punkt zawiera sie w kółku: " + c2.isPointInCircle(p1));
        System.out.println(c1.arePointsInCircle(tab));

        Point[] t = new Point[10];
        for (int i = 0; i < t.length; i++){
            t[i] = new Point(random.nextInt(10), random.nextInt(10));
        }

        for (Point p : t){
            System.out.println(p);
        }
    }
}
