package podstawy4;

public class Points {

    private Point[] data;

    public Points() {
        data = new Point[3];
    }

    public Points(int size) {
        data = new Point[size];
    }

    public Points(Points p) {
        data = new Point[p.data.length];
        for (int i = 0; i < p.data.length; i++) {
            data[i] = new Point(p.data[i]);
        }
    }

    public Point getPoint(int i) {
        return new Point(data[i]);
    }

    public void setPoint(int i, Point p){
        //data[i].set(p.getX1(), p.getY1());
    }
}
