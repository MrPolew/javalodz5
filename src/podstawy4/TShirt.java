package podstawy4;

import java.util.Scanner;

public class TShirt {

    public enum TShirtSizes {
        S, M, L, XL, XXL;
    }

    public enum TShirtColor {
        RED, GREEN, BLUE, WHITE, BLACK;
    }

    public static String availableSizes() {
        String result = "<";
        for (int i = 0; i < TShirtSizes.values().length; i++) {
            result = result + TShirtSizes.values()[i];
            if (i < TShirtSizes.values().length - 1) {
                result += ", ";
            }
        }
        result += ">";
        return result;
    }

    public static String availableColors() {
        String result = "<";
        for (int i = 0; i < TShirtColor.values().length; i++) {
            result = result + TShirtColor.values()[i];
            if (i < TShirtColor.values().length - 1) {
                result += ", ";
            }
        }
        result += ">";
        return result;
    }

        private TShirtColor color;
        private TShirtSizes size;
        private String producer;
        private String model;

    public TShirtColor getColor() {
        return color;
    }

    public void setColor(TShirtColor color) {
        this.color = color;
    }

    public TShirtSizes getSize() {
        return size;
    }

    public void setSize(TShirtSizes size) {
        this.size = size;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public TShirt() {
            size = TShirtSizes.M;
            color = TShirtColor.BLACK;
            producer = "Made in China";
            model = "universal";
        }

    public TShirt(TShirtSizes size, TShirtColor colors, String producer, String model) {
            this.color = colors;
            this.size = size;
            this.producer = producer;
            this.model = model;
        }

        @Override
        public String toString () {
            return "Rozmiar: " + size +
                    " Kolor: " + color +
                    " Producent: " + producer +
                    " Model: " + model;
        }

    public boolean equals(TShirt t) {
        if(size != t.size) return false;
        else if(color != t.color) return false;
        else if(! producer.equals(t.producer)) return false;
        else if(! model.equals(t.model)) return false;
        return true;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String size, color, producer, model;

        TShirt s1 = new TShirt();
        TShirt s4 = new TShirt();
        TShirt s2 = new TShirt(TShirtSizes.XXL, TShirtColor.GREEN, "Poland", "unisex");
        System.out.println(s1);
        System.out.println(s2);

        System.out.println("Twoja wymarzona koszulka:");
        System.out.print("Rozmiar: ");
        size = in.nextLine();
        System.out.print("Kolor: ");
        color = in.nextLine();
        System.out.print("Producent: ");
        producer = in.nextLine();
        System.out.print("Model: ");
        model = in.nextLine();

        TShirt t3 = new TShirt(TShirtSizes.valueOf(size.toUpperCase()), TShirtColor.valueOf(color.toUpperCase()), producer, model);
        System.out.println(t3);
        System.out.println(availableSizes());
        System.out.println(availableColors());

        System.out.println(s1.equals(s4));
    }
}
