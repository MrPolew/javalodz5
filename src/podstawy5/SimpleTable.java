package podstawy5;

public class SimpleTable {

    private int[] numbers;

    public SimpleTable(int... numbers) {
        this.numbers = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            this.numbers[i] = numbers[i];
        }
    }

    @Override
    public String toString() {
        String result = "";
        for (int i = 0; i < numbers.length; i++) {
            result = result + numbers[i];
            if (i < numbers.length - 1) {
                result = result + ", ";
            }
        }
        return result;
    }

    public void add(int... nums) {
        numbers = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            numbers[i] = nums[i];
        }
    }

    public void remove(int... indexes) {
        if(numbers == null) {
            System.out.println("Nie usuniesz nic. Tablica jest pusta");
        } else {
            for (int i = 0; i < numbers.length; i++) {
                for (int j = 0; j < indexes.length; j++) {
                    if(numbers[i] == indexes[j]) {
                        numbers[i] = 0;
                    }
                }
            }
        }
    }

    public void addElement(int e) {
        int[] newNumbers = new int[numbers.length + 1];
        for (int i = 0; i < numbers.length; i++) {
            newNumbers[i] = numbers[i];
        }
        newNumbers[numbers.length] = e;
        numbers = newNumbers;
    }

    public void addElements(int... elems) {

        /* Nieoptymalna metoda. Caly czas tworzy nowe tablice
        for (int i = 0; i < elems.length; i++) {
            addElement(elems[i]);
        }*/

        if (elems.length == 0) return;
        int[] newNumbers = new int[numbers.length + elems.length];
        for (int i = 0; i < numbers.length; i++) {
            newNumbers[i] = numbers[i];
        }
        for (int i = 0; i < elems.length; i++) {
            newNumbers[numbers.length + i] = elems[i];
        }
        numbers = newNumbers;
    }

    public void removeElement(int e) {
        int counter = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] == e) {
                counter++;
            }
        }
        int[] newNumbers = new int[numbers.length - counter];
        int j = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] != e) {
                newNumbers[j] = numbers[i];
                j++;
            }
        }

        numbers = newNumbers;
    }

    public void removeElements(int... elems) {
        if(elems.length == 0) return;
        if(elems.length == 1) removeElement(elems[0]);

        for (int i = 0; i < numbers.length; i++) {
            for (int j = 0; j < elems.length; j++) {
                if (numbers[i] == elems[j]){
                    numbers[i] = elems[0];
                }
            }
        }
        removeElement(elems[0]);
    }

    public static void main(String[] args) {
        /*SimpleTable tab1 = new SimpleTable();
        SimpleTable tab2 = new SimpleTable();
        SimpleTable tab3 = new SimpleTable();
        tab1.add(10, 20, 30, 40, 50, 60);
        System.out.println(Arrays.toString(tab1.getNumbers()));
        tab2.add(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        System.out.println(Arrays.toString(tab2.getNumbers()));
        tab2.remove(3, 6, 9);
        System.out.println(Arrays.toString(tab2.getNumbers()));
        tab3.remove(3, 6, 9);
        System.out.println(Arrays.toString(tab3.getNumbers()));*/

        SimpleTable a = new SimpleTable();
        SimpleTable b = new SimpleTable(2, 4, 8);

        System.out.println(a);
        System.out.println(b);
        a.addElements(3, 6, 10, 12, 5);
        a.addElements();
        a.addElement(17);
        a.addElements(10, 20, 30, 40);
        System.out.println(a);
        a.removeElement(10);
        System.out.println(a);
        a.removeElements(6, 5, 30);
        System.out.println(a);
    }
}
