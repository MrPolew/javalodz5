package podstawy5.figury;

public class Figure {
    // Figura zawsze ma jeden punkt

    private int x1, y1;
    private FigureType type;

    public Figure() {
        x1 = 0;
        y1 = 0;
        type = FigureType.NONE;
    }

    public Figure(int x1, int y1, FigureType type) {
        this.x1 = x1;
        this.y1 = y1;
        this.type = type;
    }

    public int getX1() {
        return x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public int getY1() {
        return y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public FigureType getType() {
        return type;
    }

    public void setType(FigureType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "(" + x1 + "," + y1 + ") FigureType: " + type;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if (obj == null) return false;
        if(getClass() != obj.getClass()) return false;
        Figure f = (Figure) obj;
        if (this.x1 != f.x1) return false;
        else if (this.y1 != f.y1) return false;
        else if (this.type != f.type) return false;
        return  true;
    }

    public double getArea() {
        return 0.0;
    }

    public double getCircuit() {
        return 0.0;
    }

    public void getEdges() {

    }
}
