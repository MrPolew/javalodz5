package podstawy5.figury;

public enum FigureType {
    LINE, RECTANGLE, CIRCLE, SQUARE, POINT, NONE;
}
