package podstawy5.figury;

public class Line extends Figure {

    private int x2, y2;

    public Line() {
        super(0, 0, FigureType.LINE);
        x2 = 4;
        y2 = 4;
    }

    public Line(int x1, int y1, int x2, int y2) {
        super(x1, y1, FigureType.LINE);
        this.x2 = x2;
        this.y2 = y2;
    }

    @Override
    public String toString() {
       return String.format("%s------------------%s", "(" + x2 + "," + y2 + ")", super.toString());
    }
}
