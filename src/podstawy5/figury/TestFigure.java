package podstawy5.figury;

public class TestFigure {

    public static void main(String[] args) {

        Figure figure1 = new Figure();
        Figure figure2 = new Figure();
        Figure figure3 = new Figure(0, 0, FigureType.NONE);

        System.out.println(figure1);
        System.out.println(figure3);

        System.out.println(figure1.equals(figure3));

        Line line1 = new Line(10, 10, 20, 20);
        System.out.println(line1);
    }
}
