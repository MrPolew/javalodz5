package podstawy5.figuryLukasz;

public class TestFigure {

    public static void main(String[] args) {
        /*
        Figure figure1 = new Figure();
        System.out.println(figure1);

        Point point1 = new Point();
        System.out.println(point1);

        System.out.println();
        System.out.println(figure1.equals(point1));
        System.out.println(point1.equals(figure1));
        System.out.println(point1.equals(point1));

        Point point2 = new Point(3, 4);
        System.out.println(point1.equals(point2));

        Circle circle1 = new Circle(3, 4,0);
        Circle circle2 = new Circle(3, 4,5);
        System.out.println(circle1);
        System.out.println(circle2);
        System.out.println(circle1.equals(circle2));
        System.out.println(point2.equals(circle1));

        Line line1 = new Line(new Point(2, 9), new Point(2, 4));
        Line line2 = new Line(new Point(2, 9), new Point(3, 4));
        System.out.println(line1);
        System.out.println(line2);
        System.out.println(line1.equals(line2));
        System.out.println(line2.equals(line2));

        System.out.println();
        Polygon polygon = new Polygon();
        System.out.println(polygon);
        polygon.addEdge(new Point(2, 12));
        polygon.addEdge(new Point(-5, 6));
        polygon.addEdge(new Point(4, -1));
        polygon.addEdge(new Point(2, 5));
        polygon.addEdge(new Point(3, 6));
        System.out.println(polygon);
        polygon.removeEdge(4);
        System.out.println(polygon);
        polygon.setEdge(1, new Point(-10, -10));
        System.out.println(polygon);
        */

        Figure[] figures = new Figure[5];
        figures[0] = new Figure();
        figures[1] = new Point(2, 5);
        figures[2] = new Circle(2, 5, 5);
        figures[3] =new Line((Point) figures[1], new Point(2, 4));
        figures[4] = new Polygon();
        figures[4].addEdge(new Point(3, 1));
        figures[4].addEdge(new Point(7, 3));
        figures[4].addEdge(new Point(-10, -6));

        for (int i = 0; i < figures.length; i++) {
            System.out.println(figures[i]);
        }
    }
}
