package podstawy5.figuryLukaszAbstrakcyjne;

public class Line extends Figure {

    private Point start, end;

    public Line() {
        super(FigureType.LINE);
        start = new Point();
        end = new Point();
    }

    @Override
    public double getArea() {
        return 0;
    }

    @Override
    public double getCircuit() {
        return 0;
    }

    public Line(Point start, Point end) {
        super(FigureType.LINE);
        this.start = new Point(start);
        this.end = new Point(end);
        sort();
    }

    public Line(Line line) {
        this(line.start, line.end);
    }

    public Point getStart() {
        return start;
    }

    public Point getEnd() {
        return end;
    }

    public void setStart(Point start) {
        this.start = start;
    }

    public void setEnd(Point end) {
        this.end = end;
    }

    public void sort() {
        if ((start.getX() > end.getX()) || (start.getX() == end.getX() && start.getY() > end.getY())) {
            Point temp = start;
            start = end;
            end = temp;
        }
    }

    public Point[] getEdges() {
        Point[] result = new Point[2];
        result[0] = start;
        result[1] = end;
        return result;
    }

    @Override
    public void addEdge() {

    }
}
