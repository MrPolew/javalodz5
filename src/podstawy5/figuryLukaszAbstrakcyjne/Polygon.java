package podstawy5.figuryLukaszAbstrakcyjne;

public class Polygon extends Figure {

    private Point[] edges;

    public Polygon() {
        super(FigureType.POLYGON);
        edges = null;
    }

    @Override
    public double getArea() {
        return 0;
    }

    @Override
    public double getCircuit() {
        return 0;
    }

    public Polygon(Polygon p) {
        super(FigureType.POLYGON);
        edges = new Point[p.edges.length];
        for (int i = 0; i < p.edges.length; i++) {
            edges[i] = p.edges[i];
        }
    }

    public void addEdge(Point p) {
        if (edges == null) {
            edges = new Point[1];
            edges[0] = p;
            return;
        }
        Point[] new_edges = new Point[edges.length + 1];

        for (int i = 0; i < edges.length; i++) {
            new_edges[i] = edges[i];
        }
        new_edges[edges.length] = p;
        edges = new_edges;
    }

    public void removeEdge(int index) {
        if (index > edges.length) return;

        Point[] new_edges = new Point[edges.length - 1];

        for (int i = 0; i < index - 1; i++) {
            new_edges[i] = edges[i];
        }
        for (int i = index; i < edges.length; i++) {
            new_edges[i - 1] = edges[i];
        }
    }

    public Point getEdge(int index) {
        if (index > edges.length) return null;
        return edges[index - 1];
    }

    public void setEdge(int index, Point p){
        if (index > edges.length) return;
        edges[index - 1] = p;
    }

    @Override
    public Point[] getEdges() {
        return edges;
    }

    @Override
    public void addEdge() {

    }
}

