package podstawy5.warzywa;

public class PomidorMalinowy extends Pomidor {

    public PomidorMalinowy () {
        nazwa = super.nazwa + " malinowy";
    }

    @Override
    public String toString() {
        return "Super super " + nazwa;
    }
}
