package podstawy5.warzywa;

public class TestWarzywa {

    public static void main(String[] args) {
        Warzywo[] salatka = new Warzywo[9];

        salatka[0] = new Pomidor();
        salatka[1] = new Ogorek();
        salatka[2] = new Cebula();
        salatka[3] = new PomidorMalinowy();
        salatka[4] = new Cebula();

        for(Warzywo warzywo: salatka) {
            System.out.println(warzywo);
        }
    }
}
