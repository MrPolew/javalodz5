package podstawy6;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class DateOfBirth {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        DateTimeFormatter formatter= DateTimeFormatter.ofPattern("dd/MM/yyyy");
        System.out.println("Podaj datę urodzenia: (DD/MM/YYYY)");
        String date = scanner.nextLine();

        LocalDate birthday = LocalDate.parse(date, formatter);;
        LocalDate dateNow = LocalDate.now();
        Period differance = Period.between(dateNow, birthday);
        System.out.println("Twoja data urodzenia: " + birthday);
        System.out.println("Data teraz: " + dateNow);
        System.out.println("Roznica lat : " + -differance.getYears() + "\nmiesiecy: " + -differance.getMonths() + "\ndni: " + -differance.getDays());
    }
}
