package podstawy6;

import java.util.Scanner;

public class RegexParsers {

    public static boolean checkColor(String arg) {
        String regexColor = "red|green|blue|black|white";
        return arg.matches(regexColor);
    }

    public static boolean checkDate(String arg) {
        String regexDate = "(([0-2]?[1-9])|([1-3]0)|31)[-./']((0?[1-9])|(1[0-2]))[-./']((19|20)[0-9]{2})";
        return arg.matches(regexDate);
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        System.out.println("Wprowadź date: DD/MM/YYYY");
        String s = cin.nextLine();
        if (checkDate(s)) {
            System.out.println("Poprawnie. Dziękuje");
        } else {
            System.out.println("Niepoprawnie. Spróbuj jeszcze raz!");
        }
    }
}
