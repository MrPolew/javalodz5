package podstawy6;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class SkanerPlikuTekstowego {

    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("Podaj dostep do pliku:");
        Scanner scanner = new Scanner(System.in);
        String sciezka = scanner.nextLine();
        File file = new File(sciezka);
        scanner.close();
        Scanner in = new Scanner(file);

        while(in.hasNextLine()){
            String zdanie = in.nextLine();
            System.out.println(zdanie);
        }
        in.close();
    }
}
