package podstawy6;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Program przechowująca maksymalnie 10 napisów,
 * umożliwiająca ich zapis/odczyt do/z pliku tekstowego.
 *
 * @author Polew
 * @version 1.0
 */

public class StringToFile {
    /**
     * Tablica przechowująca napisy
     */
    public static String[] strings = new String[10];

    /**
     * Metoda dodająca nowy napis do tablicy.
     * Napis zostanie dodany pod warunkiem, że w tablicy jest miejsce.
     *
     * @param s napis do dodania
     */
    public static void addString(String s) {
        for (int i = 0; i < strings.length; i++) {
            if (strings[i] == null) {
                strings[i] = s;
                return;
            }
        }
    }

    /**
     * Metoda usuwająca z tablicy wszystkie wystąpienia napisu podanego jako parametr.
     * @param s napis do usunięcia
     */
    public static void removeString(String s) {
        for (int i = 0; i < strings.length; i++) {
            if (strings[i].equals(s)) {
                strings[i] = null;
                return;
            }
        }
    }

    /**
     * Metoda zapisująca tablicę do pliku podanego jako parametr. Plik musi istnieć.
     * @param filename nazwa i ścieżka do istniejącego pliku do napisu.
     * @throws FileNotFoundException
     * @see StringToFile#readFromFile(String) zobacz też wczytywanie pliku
     */
    public static void saveToFile(String filename) throws FileNotFoundException {
        PrintWriter writer = new PrintWriter(filename);
        for (int i = 0; i < strings.length; i++) {
            if (strings[i] != null) {
                writer.println(strings[i]);
            }
        }
        writer.close();
    }

    /**
     * Metoda wczytująca tablicę z pliku podanego jako parametr. Plik musi istnieć.
     * @param filename nazwa i ścieżka do istniejącego pliku do napisu.
     * @throws FileNotFoundException
     */
    public static void readFromFile(String filename) throws FileNotFoundException {
        File file = new File(filename);
        Scanner in = new Scanner(file);
        int i = 0;
        while (in.hasNextLine()) {
            strings[i] = in.nextLine();
            i++;

            if (i == strings.length) return;
        }
    }


    /**
     * Metoda wypisująca zawartość tablicy na ekran (napisy rozdzielone znakiem nowej linii)
     */
    public static void print() {
        for (int i = 0; i < strings.length; i++) {
            if (strings[i] != null) {
                System.out.println(strings[i]);
            }
        }
    }


    /**
     * Prosty moduł testujący.
     * Program wczytuję dane z pliku "strings.txt", pozwala uzytkownikowi wprowadzić dowolną ilość napisów.
     *
     * @param args standardoowy argument metody main.
     * @throws FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {
        Scanner cin = new Scanner(System.in);

        String line;

        readFromFile("C:\\Users\\Polew\\Desktop\\JavaPrograms\\SDA\\src\\podstawy6\\bca.txt");
        print();

        System.out.println();
        System.out.println("Dodaj nowe napisy");

        do {
            line = cin.nextLine();
            if (!line.equals("")) {
                addString(line);
            }
        } while (!line.equals(""));

        print();
        saveToFile("C:\\Users\\Polew\\Desktop\\JavaPrograms\\SDA\\src\\podstawy6\\bca.txt");
    }
}
