package podstawy6;

import java.time.Duration;
import java.time.Instant;
import java.util.Scanner;

public class TestPoint {

    private static String menu = "Cześć. Co chcesz zrobić?\n" +
            "1. Wprowadź dane punktu 1\n" +
            "2. Wprowadź dane punktu 2\n" +
            "3. Przesuń punkt 1 o wektor\n" +
            "4. Przesuń punkt 2 o wektor\n" +
            "5. Oblicz odległość między punktami\n" +
            "6. Wyświetl wszystkie dane\n" +
            "0. Zakończ\n";

    public static void main(String[] args) {

        Instant start = Instant.now();
        Instant end = Instant.now();
        int choice;
        Scanner cin = new Scanner(System.in);
        Point p1, p2;
        p1 = new Point();
        p2 = new Point();
        int a, b;

        do {
            System.out.print(menu);
            choice = cin.nextInt();

            switch (choice) {
                case 1:
                    System.out.print("Wprowadź X: ");
                    p1.setX(cin.nextInt());
                    System.out.print("Wprowadź Y: ");
                    p1.setY(cin.nextInt());
                    break;
                case 2:
                    System.out.print("Wprowadź X: ");
                    p2.setX(cin.nextInt());
                    System.out.print("Wprowadź Y: ");
                    p2.setY(cin.nextInt());
                    break;
                case 3:
                    System.out.print("Wprowadź X wektora: ");
                    a = cin.nextInt();
                    System.out.print("Wprowadź Y wektora: ");
                    b = cin.nextInt();
                    p1.move(a, b);
                    break;
                case 4:
                    System.out.print("Wprowadź X wektora: ");
                    a = cin.nextInt();
                    System.out.print("Wprowadź Y wektora: ");
                    b = cin.nextInt();
                    p2.move(a, b);
                    break;
                case 5:
                    System.out.println("Odległość: " + p1.distance(p2));
                    break;
                case 6:
                    System.out.println("Punkt 1: " + p1 + ", ćwiartka: " + p1.quarter());
                    System.out.println("Punkt 2: " + p2 + ", ćwiartka: " + p2.quarter());
                    break;
                case 0:
                    System.out.println("Do widzenia!");
                    end = Instant.now();
                    break;

            }

        } while (choice > 0);
        Duration d = Duration.between(start, end);
        System.out.println("Program trwał: " + d.getSeconds() + " sekund");
    }
}
