package podstawy7;

import java.util.Scanner;

public class ComputationTest {
    public static void main(String[] args) {
        double x, y;
        Computation[] computations = new Computation[2];
        computations[0] = new Exponentiation();
        computations[1] = new Multiplication();

        Scanner scanner = new Scanner(System.in);
        boolean warunek = true;
        while (warunek){
            System.out.println("Podaj x: ");
            x = scanner.nextDouble();
            System.out.println("Podaj y: ");
            y = scanner.nextDouble();
            System.out.println("Które działanie 1-Potęgowanie 2-Mnożenie: ?");
            int option = scanner.nextInt();

            switch (option) {
                case 1:
                    System.out.println(computations[0].compute(x, y));
                    break;
                case 2:
                    System.out.println(computations[1].compute(x, y));
                    break;
                case 0:
                    System.out.println("Do widzenia");
                    warunek = false;
                    break;
                default:
                    System.out.println("Nie ma takiego wyboru");
            }
        }
    }
}
