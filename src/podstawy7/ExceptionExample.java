package podstawy7;

import java.util.Scanner;

public class ExceptionExample {
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        int dzielnik, i = 0;
        int dane[] = {3, 5, 8, 0, 12, 5};

        while(true) {
            try{
                System.out.println("Wprowadź dzielnik: ");
                dzielnik = cin.nextInt();
                System.out.println("Dzielenie " + dane[i] + " / " + dzielnik + " = " + (dane[i]/dzielnik));
            } catch(ArithmeticException e) {
                System.out.println("Błąd obliczeń!!!");
            } catch(IndexOutOfBoundsException e) {
                System.out.println("Błąd zakresu!!!");
            } catch(Exception e) {
                System.out.println("Inny błąd działania");
            }
            i++;
        }
    }
}
