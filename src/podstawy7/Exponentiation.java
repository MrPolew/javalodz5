package podstawy7;

public class Exponentiation implements Computation {
    @Override
    public double compute(double x, double y) {
        double result = 0;
        for (int i = 0; i < y; i++) {
            result += x * x;
        }
        return result;
    }

}
