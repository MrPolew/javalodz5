package podstawy7;

public class Kot implements Zwierze {

    double mass;

    public double getMass() {
        return mass;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    @Override
    public String jedz() {
        return "Kot pije mleko";
    }

    @Override
    public Zwierze rozmnozSie() {
        Kot kot = new Kot();
        kot.mass = 1000;
        return kot;
    }

    @Override
    public void rosnij(int ileKg) {
        mass += ileKg;
    }
}
