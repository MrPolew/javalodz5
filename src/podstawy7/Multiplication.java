package podstawy7;

public class Multiplication implements Computation {
    @Override
    public double compute(double x, double y) {
        return x * y;
    }
}
