package podstawy7;

public class Mysz implements Zwierze {

    double mass;

    public double getMass() {
        return mass;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    @Override
    public String jedz() {
        return "Mysz je ser";
    }

    @Override
    public Zwierze rozmnozSie() {
        Mysz mysz = new Mysz();
        mysz.mass = 500;
        return mysz;
    }

    @Override
    public void rosnij(int ileKg) {
        mass += ileKg;
    }
}
