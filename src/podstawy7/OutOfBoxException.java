package podstawy7;

public class OutOfBoxException extends Exception {

    private int size;

    public OutOfBoxException(int size) {
        this.size = size;

    }

    public int getSize() {
        return size;
    }
}
