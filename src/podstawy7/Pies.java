package podstawy7;

public class Pies implements Zwierze {

    double mass;

    public double getMass() {
        return mass;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    @Override
    public String jedz() {
        return "Pies je karme";
    }

    @Override
    public Zwierze rozmnozSie() {
        Pies pies = new Pies();
        pies.mass = 700;
        return pies;
    }

    @Override
    public void rosnij(int ileKg) {
        mass += ileKg;
    }
}
