package podstawy7;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Program SimpleCalculator wykonuje cztery podstawowe działania na liczbach rzeczywistych.
 * Program ma pokazać obsługę wyjątków w funkcji main testującej.
 */

public class SimpleCalculator {
    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public SimpleCalculator(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public SimpleCalculator(){

    }

    public int sum(int x, int y) {
        return x + y;
    }

    public int subtract(int x, int y) {
        return x - y;
    }

    public int multiply(int x, int y) {
        return x * y;
    }

    public int divide(int x, int y) {
        return x / y;
    }

    public int pow(int x, int indicator ) {
        return (int) Math.pow(x, indicator);
    }

    public double sqrt(int x) {
        return Math.sqrt(x);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x, y;

        SimpleCalculator simpleCalculator = new SimpleCalculator();
        while (true) {
            try {
                System.out.println("Program jest prostym kalkulatorem podaj dwie liczby:");
                System.out.println("Podaj x:");
                x = scanner.nextInt();
                System.out.println("Podaj y:");
                y = scanner.nextInt();

                System.out.println("Liczba x = " + x + ", Liczba y = " + y);
                System.out.println("Suma liczb: " + simpleCalculator.sum(x, y));
                System.out.println("Różnica liczb: " + simpleCalculator.subtract(x, y));
                System.out.println("Mnożenie liczb: " + simpleCalculator.multiply(x, y));
                System.out.println("Dzielenie liczb: " + simpleCalculator.divide(x, y));
                System.out.println("Potęga x do y: " + simpleCalculator.pow(x, y));
                System.out.println("Potęga y do x: " + simpleCalculator.pow(y, x));
                System.out.println("Pierwiastek x stopnia 2: " + simpleCalculator.sqrt(x));
                System.out.println("Pierwiastek y stopnia 2: " + simpleCalculator.sqrt(y));
            } catch (ArithmeticException e) {
                System.err.println("Nie dziel przez zero!! Pierwiastek nie może być ujemny");
                //e.printStackTrace();
            } catch (InputMismatchException e) {
                System.err.println("Nie wprowadziłeś prawidłowych danych");
                scanner.nextLine();
                //e.printStackTrace();
            } finally {
                System.out.println();
            }
        }

    }
}
