package podstawy7;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SimpleIntegerBox {

    private int size;
    private int[] boxes;

    public SimpleIntegerBox(int size) {
        this.size = size;
        boxes = new int[size];
    }

    public int getSize() {
        return size;
    }

    public void set(int index, int value) throws NegativeNumberException, OutOfBoxException {
        if (value < 0) {
            throw new NegativeNumberException(value);
        }
        if (index > size - 1) {
            throw new OutOfBoxException(index - size);
        }

        if (index < 0) {
            throw  new OutOfBoxException(index);
        }
        boxes[index] = value;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int value : boxes) {
            result.append("[");
            result.append(value);
            result.append("]");
        }
        return result.toString();
    }

    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        SimpleIntegerBox box = new SimpleIntegerBox(5);
        int index, value;

        while (true) {
            try {
                System.out.println("Gdzie? ");
                index = cin.nextInt();
                System.out.println("Co? ");
                value = cin.nextInt();

                box.set(index, value);
                System.out.println(box);
            } catch (OutOfBoxException e) {
                System.err.println("Wyszedłeś poza pudełko o " + e.getSize() + ". Sprawdź");
                //e.printStackTrace();
            } catch (InputMismatchException e) {
                System.err.println("Wprowadziłeś nieprawidłowy znak");
                //e.printStackTrace();
                cin.nextLine();
            } catch (NegativeNumberException e) {
                System.err.println("Wprowadziłeś negatywną liczbę");
            }
        }
    }
}
