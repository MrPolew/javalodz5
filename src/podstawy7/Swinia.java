package podstawy7;

public class Swinia implements Zwierze {

    double mass;

    public double getMass() {
        return mass;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    @Override
    public String jedz() {
        return "Swinia wpierdala wszystko kwiiii";
    }

    @Override
    public Zwierze rozmnozSie() {
        Swinia swiniunia = new Swinia();
        swiniunia.mass = 10_000;
        return swiniunia;
    }

    @Override
    public void rosnij(int ileKg) {
        mass += ileKg;
    }
}
