package podstawy7;

public class TextThread extends Thread {
    private String text;
    private int miliSec;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getMiliSec() {
        return miliSec;
    }

    public void setMiliSec(int miliSec) {
        this.miliSec = miliSec;
    }

    public TextThread(String text, int miliSec) {
        this.text = text;
        this.miliSec = miliSec;
    }

    @Override
    public void run() {

        while (true) {
            try {
                Thread.sleep(miliSec);
                System.out.println(text);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        TextThread[] tt = new TextThread[5];
        tt[0] = new TextThread("HWDP", 1000);
        tt[1] = new TextThread("Bo ja tańczyć chcę", 2000);
        tt[2] = new TextThread("Jak do tego doszło nie wiem", 3000);
        tt[3] = new TextThread("Acapulco", 4000);
        tt[4] = new TextThread("Dałem gitarę, dałem samochód", 5000);

        tt[0].start();
        tt[1].start();
        tt[2].start();
        tt[3].start();
        tt[4].start();


        try {
            Thread.sleep(0000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < tt.length; i++) {
            try {
                tt[i].finalize();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }

}
}
