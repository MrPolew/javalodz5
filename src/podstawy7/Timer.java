package podstawy7;

public class Timer implements Runnable {

    private boolean finalize = false;

    public void finish() {
        finalize = true;
    }

    public void run() {
        int time = 0;
        while (!finalize) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.err.println("Wątek został przerwany");
                return;
            }
            time++;
            int minutes = time / 60;
            int sec = time % 60;
            System.out.println(minutes + ":" + sec);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Timer timer1 = new Timer();
        Thread th1 = new Thread(timer1);
        th1.start();

        Thread.sleep(500);

        Timer timer2 = new Timer();
        Thread th2 = new Thread(timer2);
        th1.start();

        Thread.sleep(10000);
        timer1.finish();
        Thread.sleep(4000);
        timer2.finish();
    }
}
