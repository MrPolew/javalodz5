package podstawy7;

public class Zoo {
    public static void main(String[] args) throws ClassNotFoundException {
        Zwierze[] zoo = new Zwierze[10];

        zoo[0] = new Kot();
        zoo[1] = new Pies();
        zoo[2] = new Pies();
        zoo[3] = new Mysz();
        zoo[4] = new Mysz();
        zoo[5] = new Mysz();
        zoo[6] = new Swinia();
        zoo[7] = new Swinia();
        zoo[8] = new Swinia();
        zoo[9] = new Swinia();

        for (Zwierze z : zoo) {
            System.out.println(z.jedz());
            z.oddychaj();

        }
    }
}
