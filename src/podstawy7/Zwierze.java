package podstawy7;

public interface Zwierze {
    String jedz();
    Zwierze rozmnozSie();
    void rosnij(int ileKg);

    default void oddychaj() {
        System.out.println("Oddycha");
    }
}
