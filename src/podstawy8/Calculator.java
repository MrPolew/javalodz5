package podstawy8;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Calculator extends JFrame implements ActionListener {

    JPanel panel1, panel2, panel3;
    JTextField textArea1, textArea2;
    JButton bAction1, bAction2, bAction3, bAction4;
    JLabel label1, label2, label3;

    public Calculator() {
        super( "Simple Calculator" );
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500, 140);
        setLocation(50,50);
        setLayout(new BorderLayout());

        panel1 = new JPanel();
        bAction1 = new JButton("+");
        bAction1.addActionListener(this);
        bAction2 = new JButton("-");
        bAction2.addActionListener(this);
        bAction3 = new JButton("*");
        bAction3.addActionListener(this);
        bAction4 = new JButton("/");
        bAction4.addActionListener(this);
        panel1.add(bAction1);
        panel1.add(bAction2);
        panel1.add(bAction3);
        panel1.add(bAction4);

        panel2 = new JPanel();
        label1 = new JLabel("Czynnik 1:");
        label2 = new JLabel("Czynnik 2:");
        textArea1 = new JTextField();
        textArea1.setPreferredSize(new Dimension(50, 20));
        textArea2 = new JTextField();
        textArea2.setPreferredSize(new Dimension(50, 20));
        panel2.add(label1);
        panel2.add(textArea1);
        panel2.add(label2);
        panel2.add(textArea2);

        panel3 = new JPanel();
        label3 = new JLabel("Wynik: -");
        panel3.add(label3);
        //label3.setPreferredSize(new Dimension(100, 20));

        add(panel2, BorderLayout.NORTH);
        add(panel1, BorderLayout.CENTER);
        add(panel3, BorderLayout.SOUTH);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            if (e.getSource().equals(bAction1)) {
                int a = Integer.parseInt(textArea1.getText());
                int b = Integer.parseInt(textArea2.getText());

                label3.setText("Wynik: " + (a + b));
            }
            if (e.getSource().equals(bAction2)) {
                int a = Integer.parseInt(textArea1.getText());
                int b = Integer.parseInt(textArea2.getText());

                label3.setText("Wynik: " + (a - b));
            }
            if (e.getSource().equals(bAction3)) {
                int a = Integer.parseInt(textArea1.getText());
                int b = Integer.parseInt(textArea2.getText());

                label3.setText("Wynik: " + (a * b));
            }
            if (e.getSource().equals(bAction4)) {
                int a = Integer.parseInt(textArea1.getText());
                int b = Integer.parseInt(textArea2.getText());

                label3.setText("Wynik: " + (a / b));
            }
        }
        catch (NumberFormatException exception) {
            label3.setText("Niepoprawne dane. Wprowadź liczby całkowite.");
        }
        catch (ArithmeticException exception) {
            label3.setText("Nie możesz dzielić przez zero!");
        }
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Calculator();
            }
        });
    }
}