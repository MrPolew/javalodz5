package podstawy8;

import javax.swing.*;

public class FrameExample {
    public static void main(String[] args) {
        JFrame window= new JFrame("Hello World!");
        window.setSize(400,600);
        JButton b= new JButton("Click me!!");
        b.setSize(100,100);
        JButton b1=new JButton("Click me too!!");
        b1.setSize(200,220);
        window.add(b);
        window.add(b1);
        window.setVisible(false);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
