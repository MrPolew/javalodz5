package podstawy8;

import javax.swing.*;

public class HelloWorld extends JFrame {
    public HelloWorld() {
        super("Hello World");
        setSize(400, 600);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {
        new HelloWorld();
    }
}
