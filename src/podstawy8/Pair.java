package podstawy8;

public class Pair<K, V> {
    private K key;
    private V value;

    public Pair(){
        key = null;
        value = null;
    }

    public Pair(K key, V value){
        this.key = key;
        this.value = value;
    }

    public Pair(Pair<K, V> pair){
        this.key = pair.key;
        this.value = pair.value;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }


    @Override
    public String toString() {
        return "(" + key + " -> " + value + ")";
    }

    @Override
    public boolean equals(Object obj) {
        Pair<K, V> pair = (Pair<K, V>) obj;
        return (key == pair.key && value == pair.value);
    }

    public static void main(String[] args) {
        Pair<Integer, Integer> pair1 = new Pair<>(5, 10);
        Pair<Integer, String> pair2 = new Pair<>(1, "Jabłko");
        Pair<String, String> pair3 = new Pair<>("lodówka", "gruszka");

        System.out.println(pair1);
        System.out.println(pair2);
        System.out.println(pair3);
    }
}
