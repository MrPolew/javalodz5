package podstawy8;

public class SimpleTable {

    private int[] data;

    public SimpleTable() {
        data = new int[10];
    }

    public void add(int value) {
        int[] newData = new int[data.length + 1];
        for (int i = 0; i < data.length; i++) {
            newData[i] = data[i];
        }
        newData[data.length] = value;
        data = newData;
    }
}
